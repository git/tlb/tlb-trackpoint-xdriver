all:tlb.c
	gcc -I/usr/local/include/pixman-1 tlb.c -fPIC --shared -o tlb_drv.so

install:
	cp tlb_drv.so /usr/local/lib/xorg/modules/input
